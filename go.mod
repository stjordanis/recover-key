module gitlab.com/heimdall/recover-key

go 1.15

replace (
	github.com/binance-chain/go-sdk => gitlab.com/thorchain/binance-sdk v1.2.2
	github.com/binance-chain/tss-lib => gitlab.com/thorchain/tss/tss-lib v0.0.0-20200723071108-d21a17ff2b2e
	github.com/tendermint/go-amino => github.com/binance-chain/bnc-go-amino v0.14.1-binance.1
)

require (
	github.com/binance-chain/go-sdk v1.2.3
	github.com/cosmos/cosmos-sdk v0.39.0
	gitlab.com/thorchain/thornode v0.16.2
	gitlab.com/thorchain/tss/go-tss v1.0.4-chaosnet
)
